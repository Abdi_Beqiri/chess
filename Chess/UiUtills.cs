﻿using ChessLogic;
using ChessLogic.Pieces;
using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Chess
{
    public class UiUtills
    {
        public static BitmapImage PieceImage(ChessGame game ,int i, int j)
        {
            var result = new BitmapImage();
            var piece = game.Board[i, j];

            if (game.Board[i,j] is Rook)
                result = piece.Color == ChessColor.Black ? new BitmapImage(new Uri("../../Images/Pieces/BlackRook.png", UriKind.Relative))
                                                         : new BitmapImage(new Uri("../../Images/Pieces/WhiteRook.png", UriKind.Relative));

            if (game.Board[i, j] is Knight)
                result = piece.Color == ChessColor.Black ? new BitmapImage(new Uri("../../Images/Pieces/BlackKnight.png", UriKind.Relative))
                                                         : new BitmapImage(new Uri("../../Images/Pieces/WhiteKnight.png", UriKind.Relative));

            if (game.Board[i, j] is Bishop)
                result = piece.Color == ChessColor.Black ? new BitmapImage(new Uri("../../Images/Pieces/BlackBishop.png", UriKind.Relative))
                                                         : new BitmapImage(new Uri("../../Images/Pieces/WhiteBishop.png", UriKind.Relative));

            if (game.Board[i, j] is Queen)
                result = piece.Color == ChessColor.Black ? new BitmapImage(new Uri("../../Images/Pieces/BlackQueen.png", UriKind.Relative))
                                                         : new BitmapImage(new Uri("../../Images/Pieces/WhiteQueen.png", UriKind.Relative));
            if (game.Board[i, j] is King)
                result = piece.Color == ChessColor.Black ? new BitmapImage(new Uri("../../Images/Pieces/BlackKing.png", UriKind.Relative))
                                                         : new BitmapImage(new Uri("../../Images/Pieces/WhiteKing.png", UriKind.Relative));
            if (game.Board[i, j] is Pawn)
                result = piece.Color == ChessColor.Black ? new BitmapImage(new Uri("../../Images/Pieces/BlackPawn.png", UriKind.Relative))
                                                         : new BitmapImage(new Uri("../../Images/Pieces/WhitePawn.png", UriKind.Relative));
            return result;
        }


        public static BitmapImage SquareImage(int i, int j)
        {
            return ChessColorUtils.BoardColor(i, j) == ChessColor.White
                ? new BitmapImage(new Uri("../../Images/Textures/Texture_White.jpg", UriKind.Relative))
                : new BitmapImage(new Uri("../../Images/Textures/Texture_Black.jpg", UriKind.Relative));
        }

        public static Color HighlightLegalMovesColor = Color.FromArgb(0x90, 0xff, 0xff, 0x90);
        public static Color CurrentPlayerAttackedPieceColor = Color.FromArgb(0x90, 0xff, 0x40, 0x40);
        public static Color OpponentAttackedPieceColor = Color.FromArgb(0x90, 0x40, 0xff, 0x40);

    }
}
