﻿using ChessLogic;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Chess
{
    public partial class MainWindow : Window
    {
        private Button[,] _buttonArray = new Button[ChessBoardReadOnly.Size, ChessBoardReadOnly.Size];
        private ChessGame _game;
        private ChessPieceSelector _pieceSelector;
        private BoardDrawingHelper _drawingHelper;

        public MainWindow()
        {
           
            InitializeComponent();

            _game = new ChessGame();
            _pieceSelector = new ChessPieceSelector();
            _drawingHelper = new BoardDrawingHelper(_buttonArray, _game);
            for (var i = 0; i < ChessBoardReadOnly.Size; i++)
                for (var j = 0; j < ChessBoardReadOnly.Size; j++)
                {
                    var btn = InitializeButton(i, j);
                    _buttonArray[i, j] = btn;
                    DataGrid.Children.Add(btn);
                }            

            SubscribeToEvents();

            RedrawAll();

            ribbon.ApplicationMenu.Visibility = Visibility.Collapsed;
        }
        
        private Button InitializeButton(int i, int j)
        {
            var btn = new Button
            {
                Padding = new Thickness(5),
                Tag = (i, j),
                Background = new ImageBrush { ImageSource = UiUtills.SquareImage(i, j) }
            };

            btn.Click += new RoutedEventHandler(ClickHandler);
            return btn;
        }

        private void CheckBoxHandler(object sender, EventArgs e)
        {
            RedrawAll();
        }

        private void ClickHandler(object sender, EventArgs e)
        {
            var (i,j) = ((int,int))((Button)sender).Tag;

            if (_pieceSelector.CanSelectPiece(_game.Board[i, j]))
            {
                _pieceSelector.SelectedPiece = _game.Board[i, j];
                return;
            }

            if (_pieceSelector.SelectedPiece != null)
            {
                var (fromRow,fromCol) = _pieceSelector.SelectedPiece.Coordinates;
                if (_game.Board.IsLegalMove(fromRow, fromCol, i, j))
                    _game.PlayMove(fromRow, fromCol, i, j);
            }
        }

        private void SelectedPieceChangedHandler(object sender, EventArgs e)
        {
            RedrawAll();
        }

        private void TurnChangedHandler(object sender, EventArgs e)
        {
            _pieceSelector.Turn = _game.Turn;
        }

        private void BoardChangedHandler(object sender, EventArgs e)
        {
            RedrawAll();
        }

        private void UndoBtn_Click(object sender, RoutedEventArgs e)
        {
            _game.UndoMove();
        }

        private void RedoBtn_Click(object sender, RoutedEventArgs e)
        {
            _game.RedoMove();
        }

        private void NewGameBtn_Click(object sender, RoutedEventArgs e)
        {
            UnsubscribeFromEvents();

            _game = new ChessGame();
            _drawingHelper = new BoardDrawingHelper(_buttonArray, _game);

            SubscribeToEvents();

            RedrawAll();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            for (var i = 0; i < ChessBoardReadOnly.Size; i++)
                for (var j = 0; j < ChessBoardReadOnly.Size; j++)
                    _buttonArray[i, j].Click -= ClickHandler;

            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            chkAttack.Click += new RoutedEventHandler(CheckBoxHandler);
            chkLegalMoves.Click += new RoutedEventHandler(CheckBoxHandler);
            chkMobility.Click += new RoutedEventHandler(CheckBoxHandler);

            _pieceSelector.SelectedPieceChanged += new EventHandler(SelectedPieceChangedHandler);
            _game.TurnChanged += new EventHandler(TurnChangedHandler);
            _game.Board.BoardChanged += new EventHandler(BoardChangedHandler);
        }

        private void UnsubscribeFromEvents()
        {
            chkAttack.Click -= CheckBoxHandler;
            chkLegalMoves.Click -= CheckBoxHandler;
            chkMobility.Click -= CheckBoxHandler;

            _pieceSelector.SelectedPieceChanged -= SelectedPieceChangedHandler;
            _game.TurnChanged -= TurnChangedHandler;
            _game.Board.BoardChanged -= BoardChangedHandler;
        }

        public void RedrawAll()
        {
            _drawingHelper.RedrawAll(chkLegalMoves.IsChecked ?? false, 
                                     chkMobility.IsChecked ?? false, 
                                     chkAttack.IsChecked ?? false,
                                     _pieceSelector.SelectedPiece);
        }


    }
}
    

