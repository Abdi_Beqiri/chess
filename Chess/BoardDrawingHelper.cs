﻿using ChessLogic;
using ChessLogic.Pieces;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;

namespace Chess
{
    public class BoardDrawingHelper
    {
        private Button[,] _buttonArray;
        private ChessGame _game;

        public BoardDrawingHelper(Button[,] buttonArray, ChessGame game)
        {
            _buttonArray = buttonArray;
            _game = game;
        }

        public void RedrawAll(bool showLegalMoves, bool showMobility, bool showAttackedPieces, IChessPiece selectedPiece)
        {
            RedrawBoardFigures();

            RedrawSquares(showMobility);

            if (showLegalMoves)
                HighLightLegalMoves(selectedPiece);

            if (showAttackedPieces)
                HighLightAttackedPieces();
        }

        private void RedrawBoardFigures()
        {
            for (var i = 0; i < ChessBoardReadOnly.Size; i++)
                for (var j = 0; j < ChessBoardReadOnly.Size; j++)
                    _buttonArray[i, j].Content = new Image { Source = UiUtills.PieceImage(_game, i, j) };
        }

        private void RedrawSquares(bool showMobility)
        {
            List<(int, int)> mobility = null;

            if (showMobility)
                mobility = _game.Board.Mobility(_game.Turn);

            for (var k = 0; k < ChessBoardReadOnly.Size; k++)
                for (var l = 0; l < ChessBoardReadOnly.Size; l++)
                {
                    _buttonArray[k, l].Background = new ImageBrush { ImageSource = UiUtills.SquareImage(k, l) };

                    if (showMobility)
                        _buttonArray[k, l].Background.Opacity =
                            _game.Board[k, l]?.Color != _game.Turn && !mobility.Contains((k, l)) ?
                            0.3 : 1.0;
                }
        }

        private void HighLightLegalMoves(IChessPiece selectedPiece)
        {
            if (selectedPiece == null)
                return;

            foreach (var (k, l) in selectedPiece.LegalMoves(_game.Board))
                _buttonArray[k, l].Background = new SolidColorBrush(UiUtills.HighlightLegalMovesColor);
        }

        private void HighLightAttackedPieces()
        {
            foreach (var (k, l) in _game.Board.AttackedPieces())
            {
                if (_game.Board[k, l]?.Color == _game.Turn)
                    _buttonArray[k, l].Background = new SolidColorBrush(UiUtills.CurrentPlayerAttackedPieceColor);
                else
                    _buttonArray[k, l].Background = new SolidColorBrush(UiUtills.OpponentAttackedPieceColor);
            }
        }
    }
}
