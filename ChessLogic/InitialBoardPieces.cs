﻿using ChessLogic.Pieces;

namespace ChessLogic
{
    public static class InitialBoardPieces
    {
        public static IChessPiece[,]  InitialPieces()
        {
            var board = new IChessPiece[ChessBoardReadOnly.Size, ChessBoardReadOnly.Size];
            for (var i = 0; i < ChessBoardReadOnly.Size; i++)
                for (var j = 0; j < ChessBoardReadOnly.Size; j++)
                    board[i, j] = InitialPiece(i, j);

            return board;
        }

        private static IChessPiece InitialPiece(int i, int j)
        {
            IChessPiece piece = null;
            var color = (i == 0 || i == 1) ? ChessColor.Black : ChessColor.White;

            if (i == 0 || i == 7)
            {
                if (j == 0 || j == 7)
                    piece = new Rook(i, j, color);
                else if (j == 1 || j == 6)
                    piece = new Knight(i, j, color);
                else if (j == 2 || j == 5)
                    piece = new Bishop(i, j, color);
                else if (j == 3)
                    piece = new Queen(i, j, color);
                else if (j == 4)
                    piece = new King(i, j, color);
            }
            else if (i == 1 || i == 6)
                piece = new Pawn(i, j, color);

            return piece;
        }
    }
}
