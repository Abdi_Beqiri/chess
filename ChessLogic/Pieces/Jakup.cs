﻿using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public class Jakup : ChessPieceBase
    {
       public Jakup(int row, int column, ChessColor color) : base(row, column, color) { }

        public override List<(int, int)> LegalMoves(ChessBoardReadOnly board)
        {
            var result = new List<(int, int)>();

            for (var i = 0; i < ChessBoard.Size; i++)
                for (var j = 0; j < ChessBoard.Size; j++)
                {
                    if(!(board[i,j] is King || board[i, j] is Jakup) && board.CanOccupy(Color, i, j))
                        result.Add((i, j));
                }
            return result;
        }


        public override object Clone()
        {
            return new Jakup(Row, Column, Color)
            {
                HasMoved = HasMoved,
                IsCheck = IsCheck
            };
        }
    }
}
