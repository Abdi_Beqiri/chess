﻿using System;
using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public abstract class ChessPieceBase : IChessPiece, ICloneable
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public ChessColor Color { get; set; }
        public bool HasMoved { get; set; }
        public bool IsCheck { get; set; } = false;

        public (int, int) Coordinates
        {
            get => (Row, Column);
            set
            {
                Row = value.Item1;
                Column = value.Item2;
            }
        }

        public ChessPieceBase() { }

        public ChessPieceBase(int i, int j, ChessColor color)
        {
            Row = i;
            Column = j;
            Color = color;
        }

        public abstract List<(int, int)> LegalMoves(ChessBoardReadOnly board);

        protected bool OccupyAndValidate(ChessBoardReadOnly board, List<(int, int)> possibleMoves, ChessColor pieceColor, int i, int j)
        {
            if (board.CanOccupy(Color, i, j))
                possibleMoves.Add((i, j));

            return board.IsPiece(i, j);
        }

        public abstract object Clone();
    }
}
