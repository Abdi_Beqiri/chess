﻿using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public class Knight : ChessPieceBase
    {
        public Knight(int row, int column, ChessColor color) : base(row, column, color) { }

        public override List<(int, int)> LegalMoves(ChessBoardReadOnly board)
        {
            var (i, j) = Coordinates;
            List<(int, int)> result = new List<(int, int)> { };
            var possibleMoves = new List<(int, int)>
            { (i + 2, j + 1), (i + 2, j - 1), (i - 2, j + 1), (i - 2, j - 1),
                (i + 1,j + 2),(i + 1, j - 2), (i - 1, j + 2), (i - 1, j - 2)};

            foreach (var(p, q) in possibleMoves)
            {
                if(board.Contains(p, q) && board.CanOccupy(Color, p, q))
                    result.Add((p, q));
            }
            return result;
        }

        public override object Clone()
        {
            return new Knight(Row, Column, Color)
            {
                HasMoved = HasMoved,
                IsCheck = IsCheck
            };
        }
    }
}
