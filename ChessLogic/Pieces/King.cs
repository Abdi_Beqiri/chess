﻿using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public class King : ChessPieceBase
    {
        public King(int row, int column, ChessColor color) : base(row, column, color) { }

        public override List<(int, int)> LegalMoves(ChessBoardReadOnly board)
        {
            var (i, j) = Coordinates;
            List<(int, int)> result = new List<(int, int)> { };
            var possibleMoves = new List<(int, int)>
            { (i + 1, j + 1), (i + 1, j ), (i + 1, j - 1), (i - 1, j + 1),
              (i - 1, j), (i, j - 1), (i, j + 1), (i - 1, j - 1)};

            foreach (var (p, q) in possibleMoves)
            {
                if (board.Contains(p, q) && board.CanOccupy(Color, p, q))
                    result.Add((p, q));
            }

            if (!HasMoved && !IsCheck && !board.IsPiece(i, j + 1) && !board.IsPiece(i, j + 2) 
                && board[i, j+3] is Rook && !board[i, j + 3].HasMoved)
                result.Add((i, j + 2));
            if (!HasMoved && !IsCheck && !board.IsPiece(i, j - 1) && !board.IsPiece(i, j - 2) && !board.IsPiece(i, j - 3)
                && board[i, j - 4] is Rook && !board[i, j - 4].HasMoved)
                result.Add((i, j - 2));
            return result;
        }

        public override object Clone()
        {
            return new King(Row, Column, Color)
            {
                HasMoved = HasMoved,
                IsCheck = IsCheck
            };
        }
    }
}
