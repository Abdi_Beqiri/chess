﻿using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public class Rook : ChessPieceBase
    {
        public Rook(int row, int column, ChessColor color) : base(row, column, color) { }

        public override List<(int, int)> LegalMoves(ChessBoardReadOnly board)
        {
            var possibleMoves = new List<(int, int)>();
            var (i, j) = Coordinates;

            for (int column = j + 1; board.Contains(i, column); column++)
                if (OccupyAndValidate(board, possibleMoves, Color, i, column))
                    break;

            for (int column = j - 1; board.Contains(i, column); column--)
                if (OccupyAndValidate(board, possibleMoves, Color, i, column))
                    break;

            for (int row = i + 1; board.Contains(row, j); row++)
                if (OccupyAndValidate(board, possibleMoves, Color, row, j))
                    break;

            for (int row = i - 1; board.Contains(row, j); row--)
                if (OccupyAndValidate(board, possibleMoves, Color, row, j))
                    break;

            return possibleMoves;
        }

        public override object Clone()
        {
            return new Rook(Row, Column, Color)
            {
                HasMoved = HasMoved,
                IsCheck = IsCheck
            };
        }
    }
}
