﻿using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public class Pawn : ChessPieceBase
    {
        public Pawn(int row, int column, ChessColor color) : base(row, column, color) { }

        public override List<(int, int)> LegalMoves(ChessBoardReadOnly board)
        {
            var result = new List<(int,int)>();
            var (i, j) = Coordinates;

            var nextRow = Color == ChessColor.Black ? i + 1 : i - 1;

            if (!board.IsPiece(nextRow, j))
                result.Add((nextRow, j));

            if (board.OccupiedByOpposite(Color,nextRow, j + 1))
                result.Add((nextRow, j + 1));

            if (board.OccupiedByOpposite(Color, nextRow, j - 1))
                result.Add((nextRow, j - 1));

            if (HasMoved || board.IsPiece(nextRow, j))
                return result;

            var afterNextRow = Color == ChessColor.Black ? i + 2 : i - 2;

            if (!board.IsPiece(afterNextRow, j))
                result.Add((afterNextRow, j));

            return result;
        }

        public override object Clone()
        {
            return new Pawn(Row, Column, Color)
            {
                HasMoved = HasMoved,
                IsCheck = IsCheck
            };
        }
    }
}
