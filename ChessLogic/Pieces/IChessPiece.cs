﻿using System;
using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public interface IChessPiece : ICloneable
    {
        bool HasMoved { get; set; }
        bool IsCheck { get; set; }

        ChessColor Color { get; set; }

        List<(int, int)> LegalMoves(ChessBoardReadOnly board);
        (int, int) Coordinates { get; set; }
    }

}
