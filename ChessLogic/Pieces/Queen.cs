﻿using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public class Queen : ChessPieceBase
    {
        public Queen(int row, int column, ChessColor color) : base(row, column, color) { }

        public override List<(int, int)> LegalMoves(ChessBoardReadOnly board)
        {
            var possibleMoves
                = new List<(int, int)>();

            var (i, j) = Coordinates;

            // Rook moves
            for (int column = j + 1; board.Contains(i, column); column++)
                if (OccupyAndValidate(board, possibleMoves, Color, i, column))
                    break;

            for (int column = j - 1; board.Contains(i, column); column--)
                if (OccupyAndValidate(board, possibleMoves, Color, i, column))
                    break;

            for (int row = i + 1; board.Contains(row, j); row++)
                if (OccupyAndValidate(board, possibleMoves, Color, row, j))
                    break;

            for (int row = i - 1; board.Contains(row, j); row--)
                if (OccupyAndValidate(board, possibleMoves, Color, row, j))
                    break;

            // Bishop moves
            for (var k = 1; board.Contains(i + k, j + k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i + k, j + k))
                    break;

            for (var k = 1; board.Contains(i - k, j - k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i - k, j - k))
                    break;

            for (var k = 1; board.Contains(i + k, j - k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i + k, j - k))
                    break;

            for (var k = 1; board.Contains(i - k, j + k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i - k, j + k))
                    break;

            return possibleMoves;
        }

        public override object Clone()
        {
            return new Queen(Row, Column, Color)
            {
                HasMoved = HasMoved,
                IsCheck = IsCheck
            };
        }
    }
}
