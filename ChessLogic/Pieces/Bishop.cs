﻿using System.Collections.Generic;

namespace ChessLogic.Pieces
{
    public class Bishop : ChessPieceBase
    {
        public Bishop(int row, int column, ChessColor color) : base(row, column, color) { }

        public override List<(int, int)> LegalMoves(ChessBoardReadOnly board)
        {
            var possibleMoves = new List<(int, int)>();
            var (i, j) = Coordinates;

            for (var k = 1; board.Contains(i + k, j + k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i + k, j + k))
                    break;

            for (var k = 1; board.Contains(i - k, j - k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i - k, j - k))
                    break;

            for (var k = 1; board.Contains(i + k, j - k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i + k, j - k))
                    break;

            for (var k = 1; board.Contains(i - k, j + k); k++)
                if (OccupyAndValidate(board, possibleMoves, Color, i - k, j + k))
                    break;
          
            return possibleMoves;
        }

        public override object Clone()
        {
            return new Bishop(Row, Column, Color)
            {
                HasMoved = HasMoved,
                IsCheck = IsCheck
            };
        }
    }
}
