﻿using System;

namespace ChessLogic
{
    public class ChessGame
    {
        private ChessBoard _board;
        public ChessBoardReadOnly Board => _board;

        private UndoRedoManager _undoRedo;
        private ChessColor _turn;
        public ChessColor Turn
        {
            get => _turn;
            private set
            {
                _turn = value;
                OnTurnChanged(this, new EventArgs());
            }
        }

        public ChessGame(ChessBoard board = null)
        {
            _board = board ?? new ChessBoard();
            _undoRedo = new UndoRedoManager();
            _turn = ChessColor.White;
        }

        public void PlayMove(int fromRow, int fromCol, int toRow, int toCol)
        {
            var move = new ChessMove(fromRow, fromCol, toRow, toCol, _board, Turn);

            if (move.BoardAfterMove == null)
                return;

            _board.UpdateFields(move.BoardAfterMove);
            _undoRedo.AddToHistory(move);
            SwitchTurn();
        }

        public void UndoMove()
        {
            var undoState = _undoRedo.UndoMove();
            if (undoState == null)
                return;

            _board.UpdateFields(undoState);
            SwitchTurn();
        }

        public void RedoMove()
        {
            var redoState = _undoRedo.RedoMove();
            if (redoState == null)
                return;
            _board.UpdateFields(redoState);
            SwitchTurn();
        }

        private void SwitchTurn()
        {
            Turn = ChessColorUtils.Opposite(Turn);
        }

        public event EventHandler TurnChanged;

        private void OnTurnChanged(object sender, EventArgs e)
        {
            TurnChanged?.Invoke(sender, e);
        }
    }
}
