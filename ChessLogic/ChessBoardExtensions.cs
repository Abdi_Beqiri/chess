﻿using ChessLogic.Pieces;
using System.Collections.Generic;
using System.Linq;

namespace ChessLogic
{
    public static class ChessBoardExtensions
    {
        private static List<(int, int)> SimpleMobility(this ChessBoardReadOnly board, ChessColor color)
        {
            var mobility = new List<(int, int)>();
            for (var i = 0; i < ChessBoardReadOnly.Size; i++)
                for (var j = 0; j < ChessBoardReadOnly.Size; j++)
                    if (board.IsPiece(i, j) && board[i, j].Color == color)
                        mobility.AddRange(board.LegalMoves(i, j));

            return mobility;
        }

        public static  List<(int, int)> Mobility(this ChessBoardReadOnly board, ChessColor color)
        {
            var simpleMobility = board.SimpleMobility(color);
            //TODO: Need to consider case when have check or pinned pieces

            return simpleMobility;
        }

        public static IEnumerable<(int, int)> AttackedPieces(this ChessBoardReadOnly board)
        {
            return board.SimpleMobility(ChessColor.White)
                .Union(board.SimpleMobility(ChessColor.Black))
                .Where(x => board.IsPiece(x.Item1, x.Item2));
        }

        public static bool IsCheck(this ChessBoardReadOnly board, ChessColor color)
        {
            var king = board.GetPiece(new King(int.MinValue, int.MinValue, color));
            var otherMobility = board.SimpleMobility(ChessColorUtils.Opposite(color));

            king.IsCheck = otherMobility.Contains(king.Coordinates);
            return otherMobility.Contains(king.Coordinates);
        }
    }
}
