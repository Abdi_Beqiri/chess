﻿using ChessLogic.Pieces;
using System;

namespace ChessLogic
{
    public class ChessBoard : ChessBoardReadOnly, ICloneable
    {
        public ChessBoard(ChessBoardReadOnly board = null) : base(board) {}
        
        public void UpdateFields(ChessBoardReadOnly board)
        {
            if (board == null)
                throw new ArgumentException("Cannot get state from null board!");

            for (var i = 0; i < Size; i++)
                for (var j = 0; j < Size; j++)
                    _board[i, j] = board[i, j];
            OnBoardChanged(this, new EventArgs());
        }

        public new IChessPiece this[int row, int column]
        {
            get => Contains(row, column) ? _board[row, column] : null;
            set
            {
                if (!Contains(row, column))
                    return;
                _board[row, column] = value;
                OnBoardChanged(this, new EventArgs());
            }
        }

        public object Clone()
        {
            return new ChessBoard(this);
        }
    }
}