﻿using ChessLogic.Pieces;

namespace ChessLogic
{
    public class ChessMove
    {
        public readonly int FromRow;
        public readonly int FromCol;
        public readonly int ToRow;
        public readonly int ToCol;

        private readonly ChessColor Turn;
        public readonly ChessBoardReadOnly BoardBeforeMove;

        private bool _bordAfterEvaluated = false;
        private ChessBoard _boardAfterMove;

        public ChessBoard BoardAfterMove
        {
            get
            {
                if (!_bordAfterEvaluated)
                    Play();
                return _boardAfterMove;
            }
        }

        private ChessBoard _moveCheckingBoard;

        public ChessMove(int fromRow, int fromCol, int toRow, int toCol, ChessBoard board, ChessColor turn)
        {
            FromRow = fromRow;
            FromCol = fromCol;
            ToRow = toRow;
            ToCol = toCol;
            BoardBeforeMove = new ChessBoard(board);
            _moveCheckingBoard = new ChessBoard(board);
            _boardAfterMove = new ChessBoard(board);
            Turn = turn;
        }

        private void Play()
        {
            _bordAfterEvaluated = true;
            if (!CanPlayMove())
            {
                _boardAfterMove = null;
                return;
            }
            PlayMove();
        }

        private bool CanPlayMove()
        {
            if (IsShortCastling(FromRow, FromCol, ToRow, ToCol))
            {
                if (!CanPlayMove(FromRow, FromCol, ToRow, FromCol + 1)
                 || !CanPlayMove(FromRow, FromCol, ToRow, FromCol + 2))
                    return false;
            }

            if (IsLongCastling(FromRow, FromCol, ToRow, ToCol))
            {
                if (!CanPlayMove(FromRow, FromCol, ToRow, FromCol - 1)
                || !CanPlayMove(FromRow, FromCol, ToRow, FromCol - 2))
                return false;
            }

            return CanPlayMove(FromRow, FromCol, ToRow, ToCol);
        }

        private bool CanPlayMove(int fromRow, int fromCol, int toRow, int toCol)
        {
            if (!BoardBeforeMove.IsLegalMove(fromRow, fromCol, toRow, toCol))
                return false;

            var fromPiece = BoardBeforeMove[fromRow, fromCol].Clone() as IChessPiece;

            fromPiece.Coordinates = (toRow, toCol);

            _moveCheckingBoard.UpdateFields(BoardBeforeMove);

            _moveCheckingBoard[toRow, toCol] = fromPiece;
            _moveCheckingBoard[fromRow, fromCol] = null;
           
            return !_moveCheckingBoard.IsCheck(Turn);
        }

        private void PlayMove()
        {
            if (IsShortCastling(FromRow, FromCol, ToRow, ToCol))
            {
                PlayMove(FromRow, FromCol, ToRow, FromCol + 2);
                PlayMove(FromRow, FromCol + 3, ToRow, FromCol + 1);
                return;
            }

            if (IsLongCastling(FromRow, FromCol, ToRow, ToCol))
            {
                PlayMove(FromRow, FromCol, ToRow, FromCol - 2);
                PlayMove(FromRow, FromCol - 4, ToRow, FromCol - 1);
                return;
            }
            PlayMove(FromRow, FromCol, ToRow, ToCol);
        }

        private void PlayMove(int fromRow, int fromCol, int toRow, int toCol)
        {
            var fromPiece = _boardAfterMove[fromRow, fromCol];
            var toPiece = _boardAfterMove[toRow, toCol];
            var hasMoved = fromPiece.HasMoved;

            fromPiece.Coordinates = (toRow, toCol);
            fromPiece.HasMoved = true;

            _boardAfterMove[toRow, toCol] = fromPiece;
            _boardAfterMove[fromRow, fromCol] = null;

        }

        private bool IsShortCastling(int fromRow, int fromCol, int toRow, int toCol)
        {
            var king = BoardBeforeMove[fromRow, fromCol] as King;
            var rook = BoardBeforeMove[fromRow, fromCol + 3] as Rook;

            //Short castle o-o
            return (fromRow == 0 || fromRow == 7) && fromCol == 4 && toCol == 6
                 && king != null && !king.HasMoved && rook != null && !rook.HasMoved;
        }

        private bool IsLongCastling(int fromRow, int fromCol, int toRow, int toCol)
        {
            var king = BoardBeforeMove[fromRow, fromCol] as King;
            var rook = BoardBeforeMove[fromRow, fromCol - 4] as Rook;

            return (fromRow == 0 || fromRow == 7) && fromCol == 4 && toCol == 2
               && king != null && !king.HasMoved && rook != null && !rook.HasMoved;
        }
    }
}
