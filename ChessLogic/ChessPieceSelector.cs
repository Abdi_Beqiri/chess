﻿using ChessLogic.Pieces;
using System;

namespace ChessLogic
{
    public class ChessPieceSelector
    {
        public ChessColor _turn;
        public ChessColor Turn
        {
            get => _turn;
            set
            {
                _turn = value;
                SelectedPiece = null;
            }
        }

        public IChessPiece _selectedPiece;
        public IChessPiece SelectedPiece
        {
            get => _selectedPiece;
            set
            {
                _selectedPiece = CanSelectPiece(value) ? value : null;
                OnSelectedPieceChenged(this, new EventArgs());
            }
        }

        public bool CanSelectPiece(IChessPiece piece)
        {
            return piece != null && piece.Color == Turn;
        }

        public void Reset()
        {
            SelectedPiece = null;
        }

        public event EventHandler SelectedPieceChanged;

        private void OnSelectedPieceChenged(object sender, EventArgs e)
        {
            SelectedPieceChanged?.Invoke(sender, e);
        }
    }
}
