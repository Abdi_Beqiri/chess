﻿using ChessLogic.Pieces;
using System;
using System.Collections.Generic;

namespace ChessLogic
{
    public class ChessBoardReadOnly
    {
        public const int Size = 8;

        protected readonly IChessPiece[,] _board;

        public ChessBoardReadOnly(ChessBoardReadOnly board = null)
        {
            if (board == null)
            {
                _board = InitialBoardPieces.InitialPieces();
                return;
            }

            _board = new IChessPiece[Size, Size];
            for (var i = 0; i < Size; i++)
                for (var j = 0; j < Size; j++)
                    _board[i, j] = board[i, j]?.Clone() as IChessPiece;
        }

        public IChessPiece this[int row, int column]
        {
            get => Contains(row, column) ? _board[row, column] : null;
        }

        public bool OccupiedByOpposite(ChessColor color, int i, int j)
        {
            return this[i, j] != null && this[i, j].Color != color;
        }

        public bool CanOccupy(ChessColor color, int i, int j)
        {
            return this[i, j] == null || this[i, j].Color != color;
        }

        public bool Contains(int i, int j)
        {
            return i >= 0 && i < Size && j >= 0 && j < Size;
        }

        public bool IsPiece(int i, int j)
        {
            return this[i, j] != null;
        }

        public List<(int, int)> LegalMoves(int i, int j)
        {
            if (this[i, j] == null)
                new List<(int, int)>();

            return this[i, j].LegalMoves(this);
        }

        public bool IsLegalMove(int fromRow, int fromCol, int toRow, int toCol)
        {
            if (_board[fromRow, fromCol] == null || fromRow == toRow && fromCol == toCol)
                return false;

            return LegalMoves(fromRow, fromCol).Contains((toRow, toCol));
        }

        public IChessPiece GetPiece<T>(T piece)
            where T : IChessPiece
        {
            if (piece == null)
                return null;

            for (var i = 0; i < Size; i++)
                for (var j = 0; j < Size; j++)
                    if (_board[i, j] is T && _board[i, j].Color == piece.Color)
                        return _board[i, j];

            return null;
        }

        public event EventHandler BoardChanged;
        protected void OnBoardChanged(object sender, EventArgs e)
        {
            BoardChanged?.Invoke(sender, e);
        }
    }
}