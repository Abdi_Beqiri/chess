﻿using System.Collections.Generic;

namespace ChessLogic
{
    public class UndoRedoManager
    {
        int _currentMove;
        private List<ChessMove> _history;

        public UndoRedoManager()
        {
            _history = new List<ChessMove>();
            _currentMove = -1;
        }

        public void Clear()
        {
            _history.Clear();
            _currentMove = -1;
        }

        public void AddToHistory(ChessMove move)
        {
            if (HasRedoMoves())
                ClearFutureRedoMoves();
            
            _history.Add(move);
            _currentMove = _history.Count-1;
        }

        public ChessBoardReadOnly UndoMove()
        {
            if (!HasUndoMoves())
                return null;

            _currentMove--;
            return _history[_currentMove + 1].BoardBeforeMove;
        }

        public ChessBoardReadOnly RedoMove()
        {
            if (!HasRedoMoves())
                return null;

            _currentMove++;
            return _history[_currentMove].BoardAfterMove;
        }

        private void ClearFutureRedoMoves()
        {
            if (!HasRedoMoves())
                return;

            _history.RemoveRange(_currentMove + 1, _history.Count - 1 - _currentMove);
        }

        private bool HasRedoMoves()
        {
            return _currentMove < _history.Count - 1;
        }

        private bool HasUndoMoves()
        {
            return _currentMove >= 0;
        }

        
    }
}
