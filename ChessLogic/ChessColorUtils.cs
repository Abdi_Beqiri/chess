﻿namespace ChessLogic
{
    public enum ChessColor
    {
        White,
        Black
    }

    public static class ChessColorUtils
    {
        public static ChessColor Opposite(ChessColor color)
        {
            return color == ChessColor.White ? ChessColor.Black : ChessColor.White;
        }

        public static ChessColor BoardColor(int i, int j)
        {
            return (i % 2 == 0 && j % 2 == 0) || (i % 2 == 1 && j % 2 == 1) ?
                ChessColor.White : ChessColor.Black;
        }
    }
}
